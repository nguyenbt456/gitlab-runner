# Gitlab Runner

## Prepare

- [Docker](https://docs.docker.com/engine/install/ubuntu/)
- [Docker-compose](https://docs.docker.com/compose/install/)
- [Gitlab-runner image](https://hub.docker.com/r/gitlab/gitlab-runner)

## Install

- Run container

    ```shell
    $ docker-compose -f docker-compose.yml up -d
    ```

- Regiser runner

    ```shell
    $ docker exec -it gitlab-runner /bin/sh
    $ gitlab-runner register
    ```
    
    After that, provide some information to register for gitlab-runner